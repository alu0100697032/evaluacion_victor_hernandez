﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CambiarColor : MonoBehaviour {

    public List<GameObject> cubos;
    private List<Color> colores;
    private int colorSeleccionado;
	// Use this for initialization
	void Start () {
        colorSeleccionado = 0;
        colores = new List<Color>();
        colores.Add(Color.red);
        colores.Add(Color.blue);
        colores.Add(Color.green);
        colores.Add(Color.yellow);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space") == true)
        {
            for(int i = 0; i < cubos.Count; i++)
            {
                cubos[i].GetComponent<Renderer>().material.color = colores[colorSeleccionado];
            }
            if ((colorSeleccionado + 1) >= colores.Count)
                colorSeleccionado = 0;
            else
                colorSeleccionado++;
        }
    }
}
