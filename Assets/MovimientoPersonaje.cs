﻿using UnityEngine;
using System.Collections;

public class MovimientoPersonaje : MonoBehaviour {

    public int velocidad = 10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Input.GetAxis("Horizontal") * velocidad * Time.deltaTime, 0, Input.GetAxis("Vertical") * velocidad * Time.deltaTime);
    }
}
